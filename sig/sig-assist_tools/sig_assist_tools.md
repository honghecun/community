# SIG_AssistTools
English | [简体中文](./sig_assist_tools_cn.md)

Note: The content of this SIG follows the convention described in OpenHarmony's PMC Management Charter [README](/zh/pmc.md).

## SIG group work objectives and scope

### work goals
Based on the openharmony standard system, some assist tools to improve development efficiency are launched.

### work scope
1. Provides power on animation generation tools
2. Provide NAPI framework code generation tools for developers and IDE platforms of mainstream operating systems
3. Provide usage examples
4. Provide continuous improvement and optimization

### The repository 
- repository :

    - assist_tools: https://gitee.com/openharmony-sig/assist_tools

## SIG Members

### Leader
- [@zhaojunxia2020](https://gitee.com/zhaojunxia2020)

### Committers
- [@bayanxing](https://gitee.com/bayanxing)

### Meetings
 - Meeting time：time：Biweek Wednesday 19:00, UTC+8
 - Meeting application: [SIG-AssitTools application](https://shimo.im/sheets/ppCXWxYr68k3JPk9/MODOC)
 - Meeting link: Welink Meeting or Others
 - Meeting notification: [Subscribe](https://lists.openatom.io/postorius/lists/dev.openharmony.io/) mailing list dev@openharmony.io for the meeting link
 - Meeting-Minutes: [Archive link address](https://gitee.com/openharmony-sig/sig-content/tree/master/assist_tools/meetings)

### Contact (optional)

- Mailing list：dev@openharmony.io
- Zulip group：https://zulip.openharmony.cn subscript to assist_tools stream
- Wechat group：xxx



